# template-integration

# html <br/>
to rendering html file should be named as index.html<br/>
every script including should be with atribure `async` or `defer` or in the bottom on file<br/>
<br/>
The structure of bootstrap grid sould be right - <br/>
`<div class="container">`<br/>`
    <div class="row">`<br/>`
        <div class="col"></div>`<br/>`
    </div>`<br/>`
`</div>
<br/>
Use the latest html5 tag such as `<header>` `<main>` and `<footer>`<br/>

read guidness about class nav in bootstrap <br/>
 `<div class="nav">`<br/>`
        <div class="Picture1"></div>`<br/>`
        <div class="Picture2"></div>`<br/>`
        <div class="Picture3"></div>`<br/>`
        <div class="Picture4"></div>`<br/>`
        <div class="Picture5"></div>`<br/>`
        <div class="Picture6"></div>`<br/>`
 `</div> <div>
 this is incorrect structure to form a grid, try put tag `<img>`
 
 never name class with  `-` <br/>
 `class="-BL-Copy"`
 
Try to style this fragment with `display: flex` and `flex-direction: column`, this 2 line will replace all tag `<br/>` line 141 and 153

never name class like `margin`

# css <br/>

use normalize.css to remove all lack instead of writing style to `body`

try not to add only one property with class - group them or use flex grid
`.Big {
    margin-left: 196px;
}
.left {
    margin-left: 155.5px;
}
.next {
    margin-left: 17px;
}
.dropdown {
    height: 64px;
}`
<br/>
 `font-family: ProximaNova;` using should be with generic default word like `font-family: ProximaNova, serif;`

fixed width or height pixels will crash in changing resolution of the screen - try use flex grid to all css and minimize using pixels

# change all css with sass

`https://sass-lang.com/guide`

watch this scructure `https://github.com/PavloLapan/Upcode/blob/master/css/style.sass`








